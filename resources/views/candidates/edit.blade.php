@extends('layouts.app')
@section('content')
    <body class = "card text-center">
        <div class = "card-body">
            <h1>Edit candidate</h1>
            <form method = "post" action = "{{action('CandidatesController@update', $candidate->id)}}">
            @method('PATCH')
            @csrf 
            <div>
                <label for = "name" class="font-weight-bold">Candidate name</lable>
                <input type = "text" name = "name" value = {{$candidate->name}}>
            </div>
            <div>
                <label for = "email" class="font-weight-bold">Candidate email</lable>
                <input type = "text" name = "email" value = {{$candidate->email}}>
            </div>
            <div>
                <input type = "submit" class="btn btn-primary" name = "submit" value = "Update candidate">
            </div>
            </form>
        </div>
    </body>
@endsection()
