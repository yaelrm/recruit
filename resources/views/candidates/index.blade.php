@extends('layouts.app')
@section('content')
    <div><a href ="{{url('/candidates/create')}}">Add new candidate</a></div>
    <h1>List of candidates</h1>
    <table class = "table table-dark">
        <tr>
            <th>id</th><th>Name</th><th>Email</th><th>Created</th><th>Updated</th><th>Edit</th><th>Delete</th>
        </tr>
        <!-- the table data -->
        @foreach($candidates as $candidate)
            <tr>
                <td>{{$candidate->id}}</td>
                <td>{{$candidate->name}}</td>
                <td>{{$candidate->email}}</td>
                <td>{{$candidate->created_at}}</td>
                <td>{{$candidate->updated_at}}</td>
                <td><a href = "{{route('candidates.edit', $candidate->id)}}">Edit</a></td>
                <td><a href = "{{route('candidate.delete', $candidate->id)}}">Delete</a></td>
            </tr>
        @endforeach
    </table>
@endsection()
