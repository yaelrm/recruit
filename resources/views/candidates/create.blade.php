@extends('layouts.app')
@section('content')
    <body class = "card text-center">
        <div class="card-body">
            <h1>Create candidate</h1>
            <form method = "post" action = "{{action('CandidatesController@store')}}">
            @csrf 
            <div>
                <label for = "name" class="font-weight-bold">Candidate name</lable>
                <input type = "text" name = "name">
            </div>
            <div>
                <label for = "email"class="font-weight-bold">Candidate email</lable>
                <input type = "text" name = "email">
            </div>
            <div>
                <input type = "submit" class="btn btn-primary" name = "submit" value = "Create candidate">
            </div>
            </form>
        </div>
    </body>
@endsection()
